import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Model/product.dart';
import 'package:radioonthego/Screens/Menu_Screen/menu_Screen.dart';
import 'Strings.dart';

class appbar extends StatelessWidget implements PreferredSizeWidget{
  Size get preferredSize => const Size.fromHeight(60);
  const appbar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      automaticallyImplyLeading: false,
      backgroundColor: blue,
      title: Padding(
        padding: const EdgeInsets.only(top:2.0),
        child: Container(
          width: Get.width/2,
          height:50,
          color: Colors.white,
          child: Image.asset(logoImage),
        ),
      ),
      actions: [
        Padding(padding: EdgeInsets.only(right: 20),
    child: GestureDetector(
      onTap: () {
        Get.to(MenuScree());
      },
      child:  Icon(Icons.menu, color: white, size: 20,),
    ),
        ),
      ],
    );
  }
}


class menuAppBar extends StatelessWidget implements PreferredSizeWidget{
Size get preferredSize => const Size.fromHeight(60);
const menuAppBar({
Key key,
}) : super(key: key);

@override
Widget build(BuildContext context) {
  return AppBar(
    centerTitle: true,
    automaticallyImplyLeading: false,
    backgroundColor: blue,
    title: Padding(
      padding: const EdgeInsets.only(top:2.0),
      child: Container(
        width: Get.width/2,
        height:50,
        color: Colors.white,
        child: Image.asset(logoImage),
      ),
    ),
    actions: [
      Padding(padding: EdgeInsets.only(right: 20),
        child: GestureDetector(
          onTap: () {
            Get.back();
          },
          child:  Icon(Icons.close, color: white, size: 20,),
        ),
      ),
    ],
  );
}
}

Widget newsItem(Product product, Function onClick){
  return Padding(
    padding: EdgeInsets.only(left: 15, right: 15, top: 20,bottom: 5),
    child: Column(
      children: [
        GestureDetector(
          onTap: onClick,
          child: Container(
              padding: EdgeInsets.only(bottom: 5),
              width: Get.width,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54),
                  borderRadius: BorderRadius.circular(10),
                  color: white,
                  boxShadow: [
                    BoxShadow(color: lightGrey, spreadRadius: 0, blurRadius: 5,offset: Offset(3, 6),)
                  ]),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.network(
                      product.imgae,
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:8.0,right: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          setTextView(
                            product.title,
                            black,
                            13,
                            FontWeight.bold,
                          ),
                          SizedBox(height: 10),
                          setRowView(product),
                          SizedBox(height: 10),
                          setTextView(product.text, lightblack, 9,
                              FontWeight.w500),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        )
      ],
    ),
  );
}

setTextView(String title, Color color, double fontSize, fontWeight) {
  return Text(
    title,
    style: GoogleFonts.roboto(
        color: color, fontSize: fontSize, fontWeight: FontWeight.w500),
  );
}

setRowView(Product product) {
  return Row(
    children: [
      setTextView(product.date, grey, 8, FontWeight.w500),
      Container(
        height: 25,
        child: VerticalDivider(
          color: grey,
          thickness: 0.8,
        ),
      ),
      setTextView(product.time, grey, 8, FontWeight.w500),
      SizedBox(width: 5),
      setTextView(product.subtitle, blue, 8, FontWeight.w500),
    ],
  );
}


class alertsAppBar extends StatelessWidget implements PreferredSizeWidget{
Size get preferredSize => const Size.fromHeight(60);
const alertsAppBar({
Key key,
}) : super(key: key);

@override
Widget build(BuildContext context) {
  return AppBar(
    centerTitle: true,
    automaticallyImplyLeading: false,
    backgroundColor: blue,
    title: Padding(
      padding: const EdgeInsets.only(top:2.0),
      child: Container(
        width: Get.width/2,
        height:50,
        color: Colors.white,
        child: Image.asset(logoImage),
      ),
    ),
        leading: GestureDetector(
          onTap: () {Get.back();},
          child: Icon(Icons.arrow_back_ios_new_outlined, color: white, size: 20),),

    actions: [
      Padding(padding: EdgeInsets.only(right: 20),
        child: GestureDetector(
          onTap: () {
            Get.back();
          },
          child:  Icon(Icons.close, color: white, size: 20,),
        ),
      ),
    ],
  );
}
}


