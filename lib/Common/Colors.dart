import 'package:flutter/material.dart';

Color blue = Color(0xFF003AFF);
Color white = Color(0xFFFFFFFF);
Color black = Color(0xFF000000);
Color grey = Color(0xFF65666A);
Color lightblack = Color(0xFF3D3D3D);
Color lightGrey = Colors.grey.shade400;
