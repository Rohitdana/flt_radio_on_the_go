String txtRadioOntheGo = "Radio on the go";
String logoImage = "assets/images/logo.png";
String carackersImage =
    "https://s.clipartkey.com/mpngs/s/45-454098_firecracker-clipart-lighting-background-crackers-png-transparent.png";
String sportsImage = "https://www.freeiconspng.com/thumbs/no-image-icon/no-image-icon-23.jpg";    
String txtFireworks = "State offcials release safety tips\nto enjoy fireworks";
String txtDate = "JUN 28,2021";
String txtTime = "01:33:41 PM BY";
String txtBrain = "BRIAN FANCHER";
String txtAnyUsedFirework = "Any unused fireworks need to be safely stored.";
String txtIntrimCEO = "Interim C-E-O named for Hansen\nFamilay Hospital";
String txtOfficially = "Officially becomes interim C-E-O Thursday.";
String txtPayIncrease = "Pay increase for some\nemployess at the lowa Falls PD";
String txtIncreaseNumber =
    "Also approved an increase in the number of\nfirefighters at the volunteer department.";
String txtCOVID10 = "Latest COVID-10 numbers\nreleased for the State of lowa";
String txtOver45 =
    "Over 45 percent of lowa residents are now fully\nvaccinated.";
String txtMenu = "Menu";
String txtAlerts = "Alerts";
String txtContactUs = "Contact Us";
String txtSettings = "Settings";
String txtNews = "News";
String txtSports = "Sports";
String txtListen = "Listen";
String txtWeather = "Weather";
String txtMarkets = "Markets";
String txtMusic = "Music";
String txtFeature = "Features";
String txtPromotions = "Promotions";
String txtCadetsBeat = "Cabets Beat Cougars in Tuesday\nSoftball";
String txtDescriptionCadetsBeat = "Iowa Falls was the battle between the Iowa Falls-\nAldenCadets and the AGWSR Cougars in Tuesday\nsoftball action. The Cadets won the game 3-2,\ntheywere led in the batting by Makenna\nKuper with a home run.";
String txtBaseball = "AGWSR Baseball Visits Grundy\nCenter Wednesday Night";
String txtDescriptionBaseball = "The North Iowa Cedar League West race has a\nkey game on Wednesday Night, as two teams very much in the mix for the Conference Championship race do battle in Grundy Center.";
String txtAgwsr = "AGWSR, IF-A Meet in Late Season Clash";
String txtDescriptionAgwsr = "Regular Season Softball action in our area is winding down, and on Tuesday Night, KQCR brings you coverage of two area programs as the AGWSR Cougars head to Iowa Falls to take on the IF-A Cadets.";
String txtHDCAL = "H-D/CAL Bulldogs Defeat St.  Edmond Gaels On The Road";
String txtDescriptionHDCAL = "Fort Dodge was the site of Softball action Monday night as the Gaels hosted the Hampton-Dumont/CAL Bulldogs in a conference showdown.";
String txtListenNews = "Listen News";
String txtFontSize = "Font Size:";
String txtsmalle = "Small";
String txtAutoplay = "Auto-play videos over Wi-Fi";
String txtPrivacypolicy = "Privacy Policy";
String txtTermsofUse ="Terms of Use";

