
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Common/app_constant.dart';

class Product {
  final String imgae, title, date, time, subtitle, text;
  Function ontap;
  final int id;
  // final Color color;
  Product({this.id, this.subtitle, this.imgae, this.title, this.date, this.text, this.time, this.ontap});
}

List<Product> products = [
  Product(
    id: 1,
    title: txtFireworks,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtAnyUsedFirework,
    ontap: () {
     AppConstants.showWidget = true;
      print(AppConstants.showWidget);
    }
  ),
  Product(
    id: 2,
    title: txtIntrimCEO,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOfficially,
  ),
  Product(
    id: 3,
    title: txtPayIncrease,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtIncreaseNumber,
  ),
  Product(
    id: 4,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
  Product(
    id: 5,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
  Product(
    id: 6,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
];

List<Product> NewsMarker = [
  Product(
      id: 1,
      title: txtFireworks,
      date: txtDate,
      time: txtTime,
      subtitle:txtBrain,
      imgae: carackersImage,
      text: txtAnyUsedFirework,
      ontap: () {}
  ),
  Product(
    id: 2,
    title: txtIntrimCEO,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOfficially,
  ),
  Product(
    id: 3,
    title: txtPayIncrease,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtIncreaseNumber,
  ),
  Product(
    id: 4,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
  Product(
    id: 5,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
  Product(
    id: 6,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: carackersImage,
    text: txtOver45,
  ),
];

List<Product> sports = [
  Product(
      id: 1,
      title: txtCadetsBeat,
      date: txtDate,
      time: txtTime,
      subtitle:txtBrain,
      imgae: sportsImage,
      text: txtDescriptionCadetsBeat,
      ontap: () {}
  ),
  Product(
    id: 2,
    title: txtBaseball,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: sportsImage,
    text: txtDescriptionBaseball,
  ),
  Product(
    id: 3,
    title: txtAgwsr,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: sportsImage,
    text: txtDescriptionAgwsr,
  ),
  Product(
    id: 4,
    title: txtHDCAL,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: sportsImage,
    text: txtDescriptionHDCAL,
  ),
  Product(
    id: 5,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: sportsImage,
    text: txtOver45,
  ),
  Product(
    id: 6,
    title: txtCOVID10,
    date: txtDate,
    time: txtTime,
    subtitle:txtBrain,
    imgae: sportsImage,
    text: txtOver45,
  ),
];

