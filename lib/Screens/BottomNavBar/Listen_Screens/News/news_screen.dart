import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Model/product.dart';

class NewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 15, right: 15, top: 20),
              child: setTextView(txtListenNews, blue, 20, FontWeight.w500),
            ),
            Container(
              height: Get.height,
              child: ListenNews(),
            ),
          ],
        ),
      ),
    );
  }

  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(
        color: color,
        fontSize: fontSize,
        fontWeight: FontWeight.w500,
      ),
    );
  }
}

class ListenNews extends StatefulWidget {
  @override
  _ListenNewsState createState() => _ListenNewsState();
}

class _ListenNewsState extends State<ListenNews> {
  bool show = false;
  Product prod;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
            child: show
                ? showDetail()
                : ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: products.length,
                    itemBuilder: (context, index) =>
                        newsItem(products[index], () {
                      setState(() {
                        show = true;
                        prod = products[index];
                      });
                    }),
                  ),
            onWillPop: () {
              setState(() {
                show = false;
              });
              return Future.value(false);
            }));
  }

  showDetail() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        height: Get.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: white,
            boxShadow: [
              BoxShadow(
                color: lightGrey,
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(1, 1),
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(
                    prod.imgae,
                    height: 100,
                  ),
                  setTitleView()
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(prod.text),
            )
          ],
        ),
      ),
    );
  }

  setTitleView() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(prod.title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.roboto(
                  fontSize: 13, fontWeight: FontWeight.bold)),
          SizedBox(height: 15),
          setRowView(prod)
        ],
      ),
    );
  }
}
