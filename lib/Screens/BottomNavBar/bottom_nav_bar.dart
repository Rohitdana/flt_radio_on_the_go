import 'package:flutter/material.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Screens/BottomNavBar/Features_Screen/features_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Listen_Screens/listen_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Markets_Screen/markets_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Music_Screen/music_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/News_Screen/news_Screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Promotions_Screen/promotions_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Sports_Screen/sports_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Weather_Screen/weather_screen.dart';

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> with SingleTickerProviderStateMixin{
  int _selectedIndex = 0;
  TabController tabController;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  static const List<Widget> _widgetOptions = <Widget>[
    Text('Home Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Search Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Profile Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  List<String> tabNames = const<String>[
    'News',
    'Sports',
    'Listen',
    'Weather',
    'Markets',
    'Music',
    'Promotions',
    'Features',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 8);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
            color: blue,
          ),
          child: TabBar(
            isScrollable: true,
            controller: tabController,
            tabs: List.generate(tabNames.length, (index) {
              return Tab(
                text: tabNames[index],
              );
            }),
          ),
        ),
        appBar: appbar(),
        body: TabBarView(
          controller: tabController,
          children: [
            NewsScreen(),
            SportsScreen(),
            ListenScreen(),
            WeatherScreen(),
            MarketsScreen(),
            MusicScreen(),
            PromotionsScreen(),
            FeaturesScreen(),
          ],
        ),
      ),
    );
  }
}

