import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Model/product.dart';

class ReadSportsScreen extends StatefulWidget {
  @override
  _ReadSportsScreenState createState() => _ReadSportsScreenState();
}

class _ReadSportsScreenState extends State<ReadSportsScreen> {
  bool show = false;
  Product prod;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
            child: show
                ? showDetail()
                : ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: products.length,
                    itemBuilder: (context, index) =>
                        newsItem(products[index], () {
                      setState(() {
                        show = true;
                        prod = products[index];
                      });
                    }),
                  ),
            onWillPop: () {
              setState(() {
                show = false;
              });
              return Future.value(false);
            }));
  }

  showDetail() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
        height: Get.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: white,
            boxShadow: [
              BoxShadow(
                color: lightGrey,
                spreadRadius: 1,
                blurRadius: 5,
                offset: Offset(1, 1),
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.network(
                    prod.imgae,
                    height: 100,
                  ),
                  setTitleView()
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(prod.text),
            )
          ],
        ),
      ),
    );
  }

  setTitleView() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(prod.title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.roboto(
                  fontSize: 13, fontWeight: FontWeight.bold)),
          SizedBox(height: 15),
          setRowView(prod)
        ],
      ),
    );
  }
}
