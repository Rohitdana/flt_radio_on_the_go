import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Screens/BottomNavBar/Sports_Screen/Read_Sports/read_sports_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Sports_Screen/Scorecard/scorecard_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Sports_Screen/Shedules/shedules_Screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Sports_Screen/SportsBeatShow/sports_beat_show_screen.dart';

class SportsScreen extends StatefulWidget {
  @override
  _SportsScreenState createState() => _SportsScreenState();
}

class _SportsScreenState extends State<SportsScreen> with SingleTickerProviderStateMixin{
  int _selectedIndex = 0;
  TabController tabController;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  static const List<Widget> _widgetOptions = <Widget>[
    Text('Home Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Search Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Profile Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  List<String> tabNames = const<String>['Read Sports', 'Scorecard', 'Schedules', 'Sports Beat Show'];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 4);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
           Container(
             
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(0),
               color: Colors.white,
               boxShadow: [BoxShadow(
                 color: Colors.black,
                 blurRadius: 5,
                 spreadRadius: 1,
               )],
             ),
             child:  TabBar(
             indicatorColor: blue,
             indicatorPadding: EdgeInsets.only(left: 30, right: 30),
             isScrollable: true,
             controller: tabController,
             tabs: List.generate(tabNames.length, (index) {
               return Tab(
                 child:  setTextView( tabNames[index],  blue, 16, FontWeight.w500),
               );
             }),
           ),),
         Expanded(child:  TabBarView(
           controller: tabController,
           children: [ReadSportsScreen(), ScorecardScreen(), ShedulesScreen(), SportsBeatShowScreen(),],
         ),),
        ],)
      ),
    );
  }
  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(color: color,fontSize: fontSize, fontWeight: FontWeight.w500 ),

    );
  }
}

