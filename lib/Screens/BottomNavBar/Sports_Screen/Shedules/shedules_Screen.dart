

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Model/product.dart';

class ShedulesScreen extends StatefulWidget {
  @override
  _ShedulesScreenState createState() => _ShedulesScreenState();
}

class _ShedulesScreenState extends State<ShedulesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: products.length,
        itemBuilder: (context, index) => ReadNews(
          product: products[index],
        ),
      ),
    );
  }
}

class ReadNews extends StatefulWidget {
  final Product product;
  final Function press;
  const ReadNews({Key key, this.product, this.press}) : super(key: key);

  @override
  State<ReadNews> createState() => _ReadNewsState();
}

class _ReadNewsState extends State<ReadNews> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
            },
            child: Container(
                height: 165,
                width: Get.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: white,
                    boxShadow: [
                      BoxShadow(color: grey, spreadRadius: 0, blurRadius: 5)
                    ]),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Image.network(
                        widget.product.imgae,
                        height: 80,
                        width: 80,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          setTextView(
                            widget.product.title,
                            black,
                            16,
                            FontWeight.bold,
                          ),
                          SizedBox(height: 10),
                          setRowView(),
                          SizedBox(height: 10),
                          setTextView(widget.product.text, lightblack, 10,
                              FontWeight.w500),
                        ],
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }

  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(
          color: color, fontSize: fontSize, fontWeight: FontWeight.w500, ),
    );
  }

  setRowView() {
    return Row(
      children: [
        setTextView(widget.product.date, grey, 10, FontWeight.w500),
        Container(
          height: 25,
          child: VerticalDivider(
            color: grey,
            thickness: 0.8,
          ),
        ),
        setTextView(widget.product.time, grey, 10, FontWeight.w500),
        SizedBox(width: 5),
        setTextView(widget.product.subtitle, blue, 10, FontWeight.w500),
      ],
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}
