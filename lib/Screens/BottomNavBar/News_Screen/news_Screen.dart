import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Screens/BottomNavBar/News_Screen/Community_Calendar/Community_Calendar_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/News_Screen/News_Marker/Newsmarker_Screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/News_Screen/Read_News/read_news_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/News_Screen/Read_Obits/read_obits_screen.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> with SingleTickerProviderStateMixin{
  int _selectedIndex = 0;
  TabController tabController;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  static const List<Widget> _widgetOptions = <Widget>[
    Text('Home Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Search Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Profile Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  List<String> tabNames = const<String>['Read News', 'Read Obits', 'Newsmarker', 'Community Calendar',];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 4);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
           Container(
             
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(0),
               color: Colors.white,
               boxShadow: [BoxShadow(
                 color: Colors.black,
                 blurRadius: 5,
                 spreadRadius: 1,
               )],
             ),
             child:  TabBar(
             indicatorColor: blue,
             indicatorPadding: EdgeInsets.only(left: 30, right: 30),
             isScrollable: true,
             controller: tabController,
             tabs: List.generate(tabNames.length, (index) {
               return Tab(
                 child:  setTextView( tabNames[index],  blue, 16, FontWeight.w500),
               );
             }),
           ),),
         Expanded(child:  TabBarView(
           controller: tabController,
           children: [ReadNewsScreen(), ReadObitsScreen(), NewsmarkerScreen(), CommunityCalendarScreen(),],
         ),),
        ],)
      ),
    );
  }
  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(color: color,fontSize: fontSize, fontWeight: FontWeight.w500 ),

    );
  }
}

