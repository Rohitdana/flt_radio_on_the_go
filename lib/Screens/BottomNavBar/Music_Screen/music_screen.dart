


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Screens/BottomNavBar/Listen_Screens/News/News_Screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Listen_Screens/Obits/obits_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Listen_Screens/Sports/sports_screen.dart';
import 'package:radioonthego/Screens/BottomNavBar/Music_Screen/listen_music_screen.dart';

class MusicScreen extends StatefulWidget {
  @override
  _MusicScreenState createState() => _MusicScreenState();
}

class _MusicScreenState extends State<MusicScreen>
    with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;
  TabController tabController;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _widgetOptions = <Widget>[
    Text('Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Search Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    Text('Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  List<String> tabNames = const <String>['Listen Music'];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 1);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Column(
        children: [
          Container(
            width: Get.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  blurRadius: 5,
                  spreadRadius: 1,
                )
              ],
            ),
            child: Center(
              child: TabBar(
                indicatorColor: blue,
                indicatorPadding: EdgeInsets.only(left: 10, right: 10),
                isScrollable: true,
                controller: tabController,
                tabs: List.generate(tabNames.length, (index) {
                  return Tab(
                    child:
                        setTextView(tabNames[index], blue, 16, FontWeight.w500),
                  );
                }),
              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: [ListenMusicScreen()],
            ),
          ),
        ],
      )),
    );
  }

  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(
          color: color, fontSize: fontSize, fontWeight: FontWeight.w500),
    );
  }
}
