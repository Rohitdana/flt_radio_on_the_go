import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Common/app_constant.dart';

class AlertsScreen extends StatefulWidget {
  @override
  State<AlertsScreen> createState() => _AlertsScreenState();
}

class _AlertsScreenState extends State<AlertsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: alertsAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              setTextView(txtAlerts, lightblack, 16, FontWeight.w500),
              SizedBox(height: 16),
              Container(
                height: 766,
                width: Get.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: white,
                  boxShadow: [
                    BoxShadow(color: grey, blurRadius: 4, spreadRadius: 0),
                  ],
                  border: Border.all(color: black, width: 0.5),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(17.0),
                  child: Column(
                    children: [
                      seAlertsRowView(txtNews),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtSports),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtListen),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtWeather),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtWeather),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtMarkets),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtMusic),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtFeature),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                      seAlertsRowView(txtPromotions),
                      SizedBox(height: 10),
                      Divider(
                        color: black,
                        thickness: 1,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  seAlertsRowView(String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        setTextView(text, black, 17, FontWeight.w500),
        FlutterSwitch(
          height: 20.0,
          width: 50.0,
          toggleSize: 18.0,
          padding: 0.0,
          borderRadius: 20.0,
          // borderRadius: 10.0,
          activeColor: blue,
          value: AppConstants.status,
          onToggle: (value) {
            setState(() {
              AppConstants.status = value;
            });
          },
        ),
      ],
    );
  }

 
}
