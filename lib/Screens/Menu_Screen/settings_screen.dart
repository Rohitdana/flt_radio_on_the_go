import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Common/app_constant.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: alertsAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            setTextView(txtSettings, black, 17, FontWeight.w500),
            SizedBox(height: 16),
            Container(
              height: Get.height / 1.32,
              width: Get.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: white,
                boxShadow: [
                  BoxShadow(color: grey, blurRadius: 4, spreadRadius: 0),
                ],
                border: Border.all(color: black, width: 0.5),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      setTextView(txtFontSize, black, 16, FontWeight.w500),
                      SizedBox(width: 5),
                      setTextView(txtsmalle, lightblack, 16, FontWeight.normal)
                    ]),
                    SizedBox(height: 10),
                    Divider(color: black, thickness: 1),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        setTextView(txtAutoplay, black, 16, FontWeight.w500),
                        FlutterSwitch(
                          height: 20.0,
                          width: 50.0,
                          toggleSize: 18.0,
                          padding: 0.0,
                          borderRadius: 20.0,
                          // borderRadius: 10.0,
                          activeColor: blue,
                          value: AppConstants.status,
                          onToggle: (value) {
                            setState(() {
                              AppConstants.status = value;
                            });
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Divider(color: black, thickness: 1),
                    SizedBox(height: 10),
                    setTextView(txtPrivacypolicy, black, 16, FontWeight.w500),
                    SizedBox(height: 10),
                    Divider(color: black, thickness: 1),
                    SizedBox(height: 10),
                    setTextView(txtTermsofUse, black, 16, FontWeight.w500),
                    SizedBox(height: 10),
                    Divider(color: black, thickness: 1),

                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(
          color: color, fontSize: fontSize, fontWeight: FontWeight.w500),
    );
  }
}
