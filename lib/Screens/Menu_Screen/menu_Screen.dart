import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Common/Widgets.dart';
import 'package:radioonthego/Screens/Menu_Screen/alerts_screen.dart';
import 'package:radioonthego/Screens/Menu_Screen/settings_screen.dart';

class MenuScree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: menuAppBar(),
      body: Padding(padding: EdgeInsets.only(left: 16, right: 16, top: 16),
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          setTextView(txtMenu, lightblack, 16, FontWeight.w500),
          SizedBox(height: 16),
          setCommonmenuContaner(txtAlerts, () {
            Get.to(AlertsScreen());
          }),
          SizedBox(height: 16),
          setCommonmenuContaner(txtContactUs, () {},),
          SizedBox(height: 16),
          setCommonmenuContaner(txtSettings, () {
            Get.to(SettingsScreen());
          },),
        ],),
      )
    );
  }
  setTextView(String title, Color color, double fontSize, fontWeight) {
    return Text(
      title,
      style: GoogleFonts.roboto(color: color,fontSize: fontSize, fontWeight: FontWeight.w500 ),

    );
  }

  setCommonmenuContaner(String text, Function ontap) {
    return  GestureDetector(
      onTap: ontap,
      child: Container(
        height: 56,
        width: Get.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: white,
          boxShadow: [BoxShadow(color: grey, blurRadius: 4, spreadRadius: 0),],
          border: Border.all(color: black,width: 0.5),
        ),
        child: Padding(padding: EdgeInsets.only(left: 16), child:  Row(
          children: [
            Center(child: setTextView(text, black, 18, FontWeight.bold),)
          ],
        ),)
      ),
    );
  }
}
