import 'package:flutter/material.dart';
import 'package:radioonthego/Common/Colors.dart';
import 'package:get/get.dart';
import 'package:radioonthego/Common/Strings.dart';
import 'package:radioonthego/Screens/BottomNavBar/bottom_nav_bar.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5),(){
      Get.to(BottomNavBar());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blue,
      body: Container(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(80.0),
            child: Container(
                color: white,
                child: Image.asset(logoImage)),
          ),
        ),
      ),
    );
  }
}
